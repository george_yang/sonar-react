import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';
import CloneDetailApp from "./js/ClonePair/CloneDetailApp";
import CloneTaskApp from "./js/CloneTask/CloneTaskApp";

ReactDOM.render(
    <Router>
        <div>
            <Route path="/task/:projectName" component={CloneTaskApp}/>
            <Route path="/detail/:taskId" component={CloneDetailApp}/>
        </div>
    </Router>,
    document.getElementById('root'));
registerServiceWorker();
