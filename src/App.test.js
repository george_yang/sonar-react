import React from 'react';
import ReactDOM from 'react-dom';
import CloneTaskApp from "./js/CloneTask/CloneTaskApp";

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<CloneTaskApp />, div);
  ReactDOM.unmountComponentAtNode(div);
});
