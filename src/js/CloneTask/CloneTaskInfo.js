//克隆检测任务总览
import {Component} from 'react';
import React from 'react';
import { Link } from "react-router-dom";
//
export default class CloneTaskInfo extends Component{
    constructor(props){
        super(props);
        this.state={
            task : props.task,
        }
    }

    render(){
        var taskId = this.state.task.id;
        var target = "/detail/"+taskId;
        return (
            <div className="row">
                <div className="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                    {this.state.task.id}
                </div>
                <div id="topAD" className="col-lg-4 col-md-4 col-sm-4 col-xs-4" >
                     <span>{this.state.task.projectName} </span>
                </div>
                <div className="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                    {this.state.task.createTime}
                </div>
                <div className="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                    <Link to={target}><button className="btn btn-success btn-xs">查看</button></Link>
                </div>
            </div>

        )
    }
}
