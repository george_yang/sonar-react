//克隆检测任务详细
import React, {Component} from 'react';
import $ from 'jquery';
import CloneTaskInfo from './CloneTaskInfo';
import ReactPaginate from 'react-paginate';

require('bootstrap/dist/css/bootstrap.min.css');
require('../../css/style.css');

export default class CloneTaskList extends Component{

    constructor(props){
        super(props);
        this.state = {
            taskURL :"http://localhost:8080/task",
            isVisible : true,
            projectName : props.projectName,
            taskList : [],
            selectedPage : 1,
            totalPage: 0
        };
    }

    componentDidMount() {
        this.loadTaskInfoFromServer();
    }

    loadTaskInfoFromServer(){
        let url = this.state.taskURL+"/index";
        let data = {
            "projectName" : this.state.projectName,
            "page" : this.state.selectedPage
        };
        //向服务器请求检测任务列表
        $.ajax({
            url : url,
            contentType:"application/json",
            data: JSON.stringify(data),
            dataType:'json',
            type : 'post',
            success: result => {
                this.setState({
                    taskList: result.data,
                })
            },
            error: (xhr,status, err) =>{
                console.error(url, status, err.toString());
            }
        })
    }

    handlePageClick=(data)=>{
        let selecetd = data.selected;
        selecetd += 1;
        this.setState({
            selectedPage : ""+selecetd,
        },()=>{
            this.loadTaskInfoFromServer()
        });
    };
    render(){

        let taskInfoList = this.state.taskList.map((task)=> {
            return (
                <CloneTaskInfo task={task}/>
            )
        });
        return (
            <div className="col-md-8 column">
                <div className="data-div">
                    <div className="row tableHeader">
                        <div className="col-lg-1 col-md-1 col-sm-1 col-xs-1 ">
                            TaskID
                        </div>
                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            ProjectName
                        </div>
                        <div className="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                            CreateTime
                        </div>
                        <div className="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                            Option
                        </div>
                    </div>
                    <div className="tablebody">
                        <div>
                            {taskInfoList}
                        </div>
                    </div>

                </div>
                <ReactPaginate previousLabel={"previous"}
                               nextLabel={"next"}
                               breakLabel={<a href="">...</a>}
                               breakClassName={"break-me"}
                               pageCount={this.state.totalPage}
                               marginPagesDisplayed={2}
                               pageRangeDisplayed={5}
                               onPageChange={this.handlePageClick}
                               containerClassName={"pagination"}
                               subContainerClassName={"pages pagination"}
                               activeClassName={"active"} />
            </div>






        )


    }

}
