import React from 'react';
import CloneTaskList from './CloneTaskList';

require('../../css/common.css');

const CloneTaskApp = ({match}) => (
    <div id="wrap">
        <div className="leftMenu" id="leftMenu">
            <div id="rightContent">
                <div className="row clearfix">
                    <CloneTaskList projectName={match.params.projectName}/>
                </div>
            </div>
        </div>
    </div>
);

export default CloneTaskApp;
