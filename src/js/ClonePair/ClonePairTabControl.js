import {Component} from 'react';
import React from 'react';

export default class ClonePairTabControl extends Component {
    constructor(props){
        super(props);
        this.state={
            tabs:[
                {tabName : "一、二型", id : "1"},
                {tabName : "三型"   , id : "2"}
            ],
            currentIndex : "1"
        };
    }

    checkTitleIndex(index){
        return index===this.state.currentIndex ? "active" : "";
    }

    checkItemIndex(index){
        return index===this.state.currentIndex ? "tab-pane active" : "tab-pane";
    }

    render(){
        return(
            <div className="tabbable">
                {/*动态生成Tab导航*/}
                <ul className="nav nav-tabs">
                    {
                        React.Children.map( this.props.children , (element,index) => {
                            return(
                                /*箭头函数没有自己的this，这里的this继承自外围作用域，即组件本身*/
                                <li className={ this.checkTitleIndex(index) }><a onClick={ () => { this.setState({currentIndex : index}) } }>{ element.props.name }</a></li>
                            );
                        }) }
                </ul>
                {/*Tab内容区域*/}
                <div className="tab-content">
                    {React.Children.map(this.props.children,(element,index)=>{
                        return(
                            <div className={ this.checkItemIndex(index) }>{ element }</div>
                        );
                    })}
                </div>
            </div>
        );

    }




}
