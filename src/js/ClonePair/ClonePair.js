// 克隆对展示页面，按照克隆对展示代码

import {Component} from 'react';
import React from 'react';
import PrismCode from 'react-prism';

require('prismjs');
require('prismjs/themes/prism.css');

export class ClonePair extends Component{
    constructor(props) {
        //props 包含每对克隆对的代码信息，对比文件信息和id信息
        super(props);
        let pairData = props.data;
        this.state = {
            lFilePath : pairData.sourcePath,
            rFilePath : pairData.targetPath,
            lCode : pairData.sourceCode,
            rCode : pairData.targetCode
        }
    }

    render(){
        return (
            <div>
                <div className="row clearfix">
                    <div className="col-md-6 column">
                        File path: {this.state.lFilePath}
                    </div>
                    <div className="col-md-6 column">
                        File path: {this.state.rFilePath}

                    </div>
                </div>
                < div
                    className = "row clearfix" >
                    <div className = "col-md-6 column" >
                        <PrismCode component="pre" className="language-java">{this.state.lCode}</PrismCode>
                    </div>
                    <div className="col-md-6 column">
                        <PrismCode component="pre" className="language-java">{this.state.rCode}</PrismCode>
                    </div>
                </div>
            </div>
        )
    }



}
