//克隆对列表组件
import {Component} from 'react';
import React from 'react';
import ClonePairTabControl from "./ClonePairTabControl";
import ClonePairList from "./ClonePairList";
import $ from "jquery";

export default class ClonePairTabComponent extends Component{

    constructor(props){
        super(props);
        this.state = {
            taskId : this.props.taskId,
            data:[],
            isLoaded : false
        };
    }


    render(){
        if(this.state.isLoaded){
            return (
                <ClonePairTabControl>
                    <div name="一、二型">
                        <ClonePairList taskId={this.state.data.taskId} cloneType={0} totalNum={this.state.data.type12Num}/>
                    </div>
                    <div name="三型">
                        <ClonePairList taskId={this.state.data.taskId} cloneType={1} totalNum={this.state.data.type3Num}/>
                    </div>
                </ClonePairTabControl>
            );
        }else{
            return <div>Waiting...</div>
        }

    }

    componentDidMount(){
        this.loadPairNumberFromServer();
    }

    loadPairNumberFromServer(){
        let url = "http://localhost:8080/stat/view";
        let data ={
            taskId: this.props.taskId
        };
        $.ajax({
            url : url,
            data: JSON.stringify(data),
            contentType: "application/json",
            dataType: "json",
            type:"post",
            success : result =>{
                this.setState({
                    data : result.data,
                    isLoaded : true
                })
            },
            error :(xhr,status,err) =>{
                console.error(url, status, err.toString());
            }
        });
    }
}
