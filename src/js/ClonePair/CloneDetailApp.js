import React from 'react';
import CLoneStatusPanel from './CloneStatusPanel';


const CloneDetailApp = ({match}) => (

    <div className="container">
        <div className="col-lg-12">
            <CLoneStatusPanel taskId={match.params.taskId}/>
        </div>
    </div>
);

export default CloneDetailApp;
