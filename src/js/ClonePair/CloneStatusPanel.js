import React from 'react';
import $ from 'jquery';
import ClonePairTabComponent from "./ClonePairTabComponent";

export default class CLoneStatusPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cloneStatusUrl: "http://localhost:8080/stat/view",
            taskId: this.props.taskId,
            taskDetail: []
        };
        this.loadStateFromServer();
    }

    render() {
        return (
            <div>
                <div className="container-fluid">
                    <div className="row-fluid">
                        <div className="span12">
                            <dl>
                                <dt>
                                    创建时间
                                </dt>
                                <dd>
                                    {this.state.taskDetail.createTime}
                                </dd>
                                <dt>
                                    一、二型克隆对数目
                                </dt>
                                <dd>
                                    {this.state.taskDetail.type12Num}
                                </dd>
                                <dt>
                                    三型克隆对数目
                                </dt>
                                <dd>
                                    {this.state.taskDetail.type3Num}
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <ClonePairTabComponent taskId={this.state.taskId}/>
            </div>


        );
    }

    componentDidMount() {
    }

    loadStateFromServer() {
        let url = this.state.cloneStatusUrl;
        let data = {
            "taskId": this.state.taskId
        };
        $.ajax({
            url: url,
            data: JSON.stringify(data),
            contentType: "application/json",
            dataType: "json",
            type: "post",
            success: result => {
                this.setState({
                    taskDetail: result.data
                });
            },
            error: (xhr, status, err) => {
                console.error(url, status, err.toString());
            }
        })
    }


}